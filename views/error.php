<?php
ob_start();
?>

    <div class="alert alert-danger">
        <h1 class="text-center"><?= $title ?></h1>
    </div>

<?php
$content = ob_get_clean();
require_once "template.php";