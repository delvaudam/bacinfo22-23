<?php
ob_start();
?>
    <div class="container-fluid">
        <h1 class="text-center"><?= $title ?></h1>
        <!-- Subscribing Form -->
        <form action="?page=sinscrire" method="post" enctype="multipart/form-data">
            <div class="form-group p-1">
                <label for="s-login">Votre login</label>
                <input type="text" name="login" id="s-login" class="form-control" required>
                <span id="s-log-msg"></span>
            </div>

            <div class="form-group p-1">
                <label for="s-password">Votre mot de passe</label>
                <input type="password" name="password" id="s-password" class="form-control" required>
                <span id="s-pass-msg"></span>
            </div>

            <div class="form-group p-1">
                <label for="s-password2">Retaper votre mot de passe</label>
                <input type="password" name="password2" id="s-password2" class="form-control" required>
                <span id="s-pass2-msg"></span>
            </div>

            <div class="form-group p-1">
                <label for="s-email">Votre email</label>
                <input type="email" name="email" id="s-email" class="form-control" placeholder="ex: abcd@efg.xyz"
                       required>
                <span id="s-mail-msg"></span>
            </div>

            <div class="form-group p-1">
                <label for="s-lastname">Votre nom</label>
                <input type="text" name="lastname" id="s-lastname" class="form-control" required>
            </div>

            <div class="form-group p-1">
                <label for="s-firstname">Votre prénom</label>
                <input type="text" name="firstname" id="s-firstname" class="form-control" required>
            </div>

            <div class="form-group p-1">
                <label for="s-address">Votre adresse</label>
                <input type="text" name="address" id="s-address" class="form-control" required>
            </div>

            <div class="form-group p-1">
                <label for="s-nationality">Votre nationalité</label>
                <select type="text" name="nationality" id="s-nationality" class="form-select" required>
                    <option value="null">Choisissez...</option>
                    <?php foreach ($countries as $country) : ?>
                        <option value="<?= $country["id"] ?>"><?= $country["nom_fr_fr"] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="form-group p-1">
                <label for="s-telephone">Votre numéro de téléphone</label>
                <input type="tel" name="telephone" id="s-telephone" class="form-control"
                       placeholder="ex: 012/34.56.78 ou 0123/45.67.89" required>
            </div>

            <div class="form-group p-1">
                <label for="s-birth">Votre date de naissance</label>
                <input type="date" name="birthdate" id="s-birth" class="form-control" required>
            </div>

            <div class="form-group p-1">
                <label for="photo">Votre photo (facultative)</label>
                <input type="file" name="photo" id="photo" class="form-control">
            </div>

            <div class="form-group">
                <label for="condition">Cochez cette case si vous acceptez nos conditions : </label>
                <input type="checkbox" class="form-check-inline" id="condition" name="condition" value="true" required>
                <br>
                <span>Vous pouvez retrouver nos conditions en cliquant sur ce
                    <a href="#" data-bs-toggle="modal" data-bs-target="#condition-modal">lien</a>
                </span>
            </div>

            <div class="form-group text-center mt-2">
                <button type="submit" class="green-btn border-0">S'inscrire</button>
            </div>
        </form>
        <!-- End Subscribing Form -->
    </div>

    <!-- Modal GCU -->
    <div class="modal" id="condition-modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title">Nos conditions</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <?php include "conditions-include.php"; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="green-btn border-0" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal GCU -->

<?php
$content = ob_get_clean();
require_once "template.php";