<?php
ob_start();
?>

    <!-- ======= Hero Section ======= -->
    <section id="hero" class="d-flex justify-content-center align-items-center">
        <div class="container position-relative" data-aos="zoom-in" data-aos-delay="100">
            <h1>Lorem ipsum dolor sit amet.<br>Lorem ipsum dolor.</h1>
            <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium alias culpa deserunt ducimus eos
                illum, nostrum qui totam velit.</h2>
            <a href="?page=cours" class="btn-get-started">Commencer</a>
        </div>
    </section><!-- End Hero -->

    <div class="container-fluid p-2 mt-5">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aspernatur aut autem consequuntur, dolore
            dolorum
            earum eos ex excepturi exercitationem fugiat harum labore maiores nam neque praesentium, provident similique
            ullam voluptate voluptatem. Atque aut dolore eveniet fuga numquam quas rem similique sunt ullam voluptatum.
            Accusamus asperiores ex facere facilis laboriosam sapiente tempora ullam. Ab at autem blanditiis eos et
            fugit
            laborum maiores nisi, nostrum vel. Assumenda commodi cumque debitis, dolor enim error eum illo iure
            laboriosam
            laborum minima natus neque ratione saepe sed. Nulla, voluptatem.</p>
        <p>Adipisci aliquam consequuntur eaque nemo non porro quae quaerat. A accusamus adipisci at, cumque error
            explicabo
            impedit maiores modi nemo quis quod repellendus velit voluptatibus? Adipisci alias dolore dolorem dolorum
            error,
            ipsa iste itaque nesciunt pariatur quas sint sit tempore voluptates! Architecto dignissimos harum ipsum
            quidem
            veritatis? Blanditiis minus quos sequi. Accusamus ad delectus dolorum ducimus facilis impedit iusto libero
            natus
            nihil odio officia perferendis praesentium quasi, quod quos! Alias aliquam animi deleniti dicta dignissimos
            dolorum eos excepturi harum inventore, iusto nulla possimus rem soluta.</p>
        <p>Aliquam blanditiis commodi dolorum expedita soluta, veritatis? Adipisci corporis debitis earum eveniet
            exercitationem ipsum laboriosam natus nemo nulla, perferendis quod suscipit veniam. Deleniti eos et incidunt
            ipsum nostrum recusandae, reprehenderit vero. Aperiam commodi dignissimos error laborum quibusdam? Dolorum
            eos
            exercitationem ipsum maiores perspiciatis quia sit veniam. At aut consequatur enim explicabo harum hic
            impedit
            iste iusto neque, officiis praesentium quibusdam reiciendis repudiandae temporibus veritatis. Ad, aliquid
            autem
            consequuntur, est facere libero maiores molestias nobis non omnis placeat porro provident quas quasi quis,
            repellat ullam veritatis?</p>
    </div>

<?php
$content = ob_get_clean();
require_once "template.php";