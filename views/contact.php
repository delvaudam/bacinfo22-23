<?php
ob_start();
?>

    <div class="container-fluid">
        <h1 class="text-center"><?= $title ?></h1>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem cupiditate delectus, dolores ex
            exercitationem, expedita explicabo maxime minus nostrum nulla perspiciatis, possimus provident reiciendis
            saepe sequi similique sit soluta voluptatibus.</p>

        <form action="" method="post">
            <div class="form-group">
                <label for="name">Votre nom</label>
                <input type="text" class="form-control" id="name" name="name">
            </div>

            <div class="form-group">
                <label for="message">Votre message</label>
                <textarea name="message" id="message" cols="30" rows="10" class="form-control"></textarea>
            </div>

            <div class="text-center mt-2">
                <input type="submit" class="green-btn border-0">
            </div>
        </form>
    </div>

<?php
$content = ob_get_clean();
require_once "template.php";