<?php
ob_start();
?>
    <div class="container-fluid">
        <h1 class="text-center">Nos Formations</h1>

        <table class="table table-responsive dataTable" id="formationTable">
            <thead>
            <tr>
                <th>Nom</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($formations as $formation) : ?>
                <tr>
                    <td><?= $formation["name"] ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php
$content = ob_get_clean();
require_once "template.php";
