<?php

use app\controllers\Role;
use app\helpers\Access;

ob_start();
?>
    <div class="container-fluid">
        <h1 class="h1 text-center mb-3">Panneau d'administration</h1>
        <div class="row g-0">
            <a class="green-btn mb-3" href="#user-collapse" data-bs-toggle="collapse" role="button"
               aria-expanded="false"
               aria-controls="user-collapse">Utilisateurs</a>
            <div class="col-12 collapse mt-2 mb-3" id="user-collapse">
                <table class="table table-borderless table-responsive">
                    <thead>
                    <tr>
                        <th>Nom d'utilisateur</th>
                        <th>Role</th>
                        <th>Création</th>
                        <th>Gestion</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($users as $user) : ?>
                        <tr>
                            <td><a href="#" class="userSelector" data-bs-toggle="modal"
                                   data-bs-content="<?= $user["id"] ?>"
                                   data-bs-target="#userModal"><?= $user["login"] ?></a></td>
                            <td><?= $user["role"] ?><?= $user["active"] == 0 ? " - inactif" : "" ?></td>
                            <td><?= $user["created"] ?></td>
                            <td>
                                <?php if (!Access::isAdmin($user["id"])) : ?>
                                    <?php if ($user["roleid"] == Role::BANNED) : ?>
                                        <a href="?page=admin&deban=<?= $user["id"] ?>">Débannir</a>
                                    <?php else: ?>
                                        <a href="?page=admin&ban=<?= $user["id"] ?>">Bannir</a>
                                    <?php endif; endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

            <a class="green-btn mb-3" href="#formation-collapse" data-bs-toggle="collapse" role="button"
               aria-expanded="false" aria-controls="formation-collapse">Formations</a>
            <div class="col-12 collapse mt-2 mb-3" id="formation-collapse">
                <button class="green-btn border-0" data-bs-target="#formationModal" data-bs-toggle="modal">Créer une
                    formation
                </button>
                <table class="table table-borderless table-responsive">
                    <thead>
                    <tr>
                        <th>Nom</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($formations as $formation) : ?>
                        <tr>
                            <td><?= $formation["name"] ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

            <a class="green-btn mb-3" href="#course-collapse" data-bs-toggle="collapse" role="button"
               aria-expanded="false" aria-controls="course-collapse">Cours</a>
            <div class="col-12 collapse mt-2 mb-3" id="course-collapse">
                <button class="green-btn border-0" data-bs-target="#courseModal" data-bs-toggle="modal">Créer un cours
                </button>
                <table class="table table-responsive table-borderless">
                    <thead>
                    <tr>
                        <th>Code</th>
                        <th>Nom</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($courses as $course) : ?>
                        <tr>
                            <td><?= $course["code"] ?></td>
                            <td><?= $course["name"] ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <!-- User Modal -->
    <div class="modal fade" id="userModal" tabindex="-1" aria-labelledby="userModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="userModalLabel"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Utilisateur : <span id="login"></span></p>
                    <p>Email : <span id="email"></span></p>
                    <p>Nom : <span id="lastname"></span></p>
                    <p>Prénom : <span id="firstname"></span></p>
                    <p>Nationalité : <span id="nom_fr_fr"></span></p>
                    <p>Date de naissance : <span id="birthdate"></span></p>
                    <p>Téléphone : <span id="telephone"></span></p>
                    <p>Adresse : <span id="address"></span></p>
                    <p>Création : <span id="created"></span></p>
                    <p>Modification : <span id="updated"></span></p>
                    <p>Dernière connexion : <span id="lastLogin"></span></p>
                    <p>Photo : <img id="photo" class="img-fluid img-avatar" src="" alt="image utilisateur"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="green-btn border-0" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End User Modal -->

    <!-- Formation Modal -->
    <div class="modal fade" id="formationModal" tabindex="-1" aria-labelledby="formationModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="formationModalLabel">Créer une formation</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="?page=admin" method="post">
                        <div class="form-group">
                            <label for="f-name">Nom</label>
                            <input type="text" id="f-name" name="f-name" class="form-control">
                        </div>
                        <div class="form-group text-center mt-3">
                            <input type="submit" class="green-btn border-0">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="green-btn border-0" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Formation Modal -->

    <!-- Course Modal -->
    <div class="modal fade" id="courseModal" tabindex="-1" aria-labelledby="courseModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="courseModalLabel">Créer un cours</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="?page=admin" method="post">
                        <div class="form-group">
                            <label for="c-name">Nom</label>
                            <input type="text" name="c-name" id="c-name" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="c-code">Code</label>
                            <input type="text" name="c-code" id="c-code" class="form-control">
                        </div>

                        <div class="form-group mt-3 text-center">
                            <input type="submit" class="green-btn border-0">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="green-btn border-0" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Course Modal -->
<?php
$content = ob_get_clean();
require_once "template.php";
