<?php

use app\helpers\Access;

ob_start();
?>

    <div class="container-fluid">
        <h1 class="text-center">Profil : <?= $_SESSION["user"]->login ?></h1>
        <a href="?page=profil&export=<?= $_SESSION["user"]->id ?>" class="green-btn">Exporter les
            données</a>
        <?php if (!Access::isAdmin($_SESSION["user"]->id)) : ?>
            <a href="?page=profil&delete=<?= $_SESSION["user"]->id ?>" class="green-btn">Supprimer le
                profil</a>
        <?php endif;
        if (Access::isAdmin($_SESSION["user"]->id)) : ?>
            <a href="?page=admin" class="green-btn">Admin</a>
        <?php endif; ?>

        <div class="container-fluid mt-3">
            <p>Nom d'utilisateur : <?= $user->login ?></p>
            <p>E-mail : <?= $user->email ?></p>
            <p>Nom : <?= $user->lastname ?></p>
            <p>Prénom : <?= $user->firstname ?></p>
            <p>Nationalité : <?= $user->nationality ?></p>
            <p>Adresse : <?= $user->address ?></p>
            <p>Date de naissance : <?= $user->birthdate ?></p>
            <p>Téléphone : <?= $user->telephone ?></p>
            <p>Date de création : <?= $user->created ?></p>
            <p>Dernière modification : <?= $user->updated ?></p>
            <p>Dernière connexion : <?= $user->lastLogin ?></p>
            <p>
                <img class="img-fluid img-avatar"
                     src="<?php if ($user->photo == null) echo "image/default.jpg"; else echo $user->photo; ?>"
                     alt="photo utilisateur">
            </p>
        </div>

        <hr class="mt-5 mb-5">

        <a class="green-btn" id="collapse-link" data-bs-toggle="collapse" href="#updateProfile" role="button"
           aria-expanded="false"
           aria-controls="updateProfile">Modifier le profil</a>

        <!-- Update profile form -->
        <div id="updateProfile" class="collapse mt-2 p-2" style="max-width: 360px;">
            <form action="?page=profil&user=<?= $_SESSION["user"]->id ?>" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="password">Modifier le mot de passe</label>
                    <input type="password" id="password" name="password" class="form-control">
                </div>
                <div class="form-group">
                    <label for="email">Modifier l'adresse e-mail</label>
                    <input type="email" class="form-control" id="email" name="email" value="<?= $user->email ?>">
                </div>
                <div class="form-group">
                    <label for="photo">Modifier la photo</label>
                    <input type="file" id="photo" name="photo" class="form-control">
                </div>
                <div class="form-group mt-2">
                    <button class="green-btn border-0" type="submit">Mettre à jour</button>
                </div>
            </form>
        </div>
        <!-- End update profile form -->

        <hr class="mt-5 mb-5">

        <!-- User's course list -->
        <?php if ($courses) : ?>
            <h2>Liste des cours</h2>
            <div>
                <table class="table table-responsive table-borderless">
                    <thead>
                    <tr>
                        <th>Code</th>
                        <th>Nom</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($courses as $c) : ?>
                        <tr>
                            <td><?= $c["code"] ?></td>
                            <td><?= $c["name"] ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
        <!-- End user's course list -->
    </div>

<?php
$content = ob_get_clean();
require_once "template.php";