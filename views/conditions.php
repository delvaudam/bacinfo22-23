<?php
ob_start();
?>

    <div class="container-fluid mt-6">
        <?php include "conditions-include.php"; ?>
    </div>

<?php
$content = ob_get_clean();
require_once "template.php";
