<?php
ob_start();
?>

    <div class="container-fluid">
        <h1 class="text-center"><?= $title ?></h1>

        <table id="courseTable" class="table table-borderless">
            <thead>
            <tr>
                <th>Cours</th>
                <th>Formation</th>
                <th>Prérequis</th>
                <?php if (isset($_SESSION["user"])) : ?>
                    <th>Inscription</th>
                <?php endif; ?>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($courses as $course) : ?>
                <tr>
                    <td><a href="#" class="courseLink" data-bs-toggle="modal" data-bs-target="#courseModal"
                           data-bs-content='<?= json_encode($course) ?>'><?= $course["course_name"] ?></a></td>
                    <td><?= $course["formation_name"] ?></td>
                    <td><?= $course["course_prereq"] ?></td>
                    <?php if (isset($_SESSION["user"])) : ?>
                        <td>
                            <?php if ($user->isEnrolled($_SESSION["user"]->id, $course["courseid"])) : ?>
                                <a href="?page=cours&unenroll=<?= $course["courseid"] ?>">Se désinscrire</a>
                            <?php else : ?>
                                <a href="?page=cours&enrol=<?= $course["courseid"] ?>">S'inscrire</a>
                            <?php endif; ?>
                        </td>
                    <?php endif; ?>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <div class="modal" id="courseModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title"></h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p><b>Cours</b> : <span id="mCourse"></span></p>
                    <p><b>Formation</b> : <span id="mFormation"></span></p>
                    <p><b>Périodes</b> : <span id="mPeriod"></span></p>
                    <p><b>Prérequis</b> : <span id="mPrereq"></span></p>
                    <p><b>Déterminant</b> : <span id="mDeter"></span></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="green-btn border-0" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<?php
$content = ob_get_clean();
require_once "template.php";
