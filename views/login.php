<?php
ob_start();
?>

    <div class="container-fluid">
        <h1 class="text-center"><?= $title ?></h1>

        <div>
            <form action="?page=login" method="post">
                <div class="form-group p-1">
                    <label for="login">Votre login</label>
                    <input type="text" name="login" id="login" class="form-control">
                </div>

                <div class="form-group p-1">
                    <label for="password">Votre mot de passe</label>
                    <input type="password" name="password" id="password" class="form-control">
                </div>

                <div class="form-group text-center p-1">
                    <button type="submit" class="green-btn border-0">Se connecter</button>
                </div>
            </form>
        </div>
        <div>
            <a href="?page=sinscrire" class="green-btn">S'inscrire</a>
        </div>
    </div>

<?php
$content = ob_get_clean();
require_once "template.php";