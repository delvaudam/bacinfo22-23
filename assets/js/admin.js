let link = $(".userSelector");
let modal = $("#userModal");

let login = $("#login");
let email = $("#email");
let lastname = $("#lastname");
let firstname = $("#firstname");
let country = $("#nom_fr_fr");
let address = $("#address");
let tel = $("#telephone");
let bdate = $("#birthdate");
let created = $("#created");
let updated = $("#updated");
let lastLog = $("#lastLogin");
let photo = $("#photo");

link.on("click", function () {
    let id = $(this).attr("data-bs-content");
    $.ajax({
        url: "?page=admin",
        method: "post",
        data: {user: id}
    }).done(function (response) {
        let data = JSON.parse(response);
        login.html(data.login);
        email.html(data.email);
        lastname.html(data.lastname);
        firstname.html(data.firstname);
        country.html(data.nationality);
        address.html(data.address);
        tel.html(data.telephone);
        bdate.html(data.birthdate);
        created.html(data.created);
        updated.html(data.updated);
        lastLog.html(data.lastLogin);
        if (data.photo === null) {
            data.photo = "assets/img/default.png";
        }
        photo.attr("src", data.photo);
    });
});