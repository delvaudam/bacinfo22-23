let s_login = $("#s-login");
let s_pass1 = $("#s-password");
let s_pass2 = $("#s-password2");

s_login.on("keyup", function () {
    let logmsg = $("#s-log-msg");
    logmsg.empty();
    let len = s_login.val().length;
    if (len < 10) {
        logmsg.css("color", "red").html("Minimum 10 caractères");
    }
});

s_pass1.on("keyup", function () {
    let passmsg = $("#s-pass-msg");
    let len = s_pass1.val().length;
    passmsg.empty();
    if (len < 10) {
        passmsg.css("color", "red").html("Minimum 10 caractères");
    }
});

s_pass2.on("keyup", function (){
    let pass2msg = $("#s-pass2-msg");
    pass2msg.empty();
    if (s_pass2.val() !== s_pass1.val()){
        pass2msg.css("color", "red").html("Les deux mots de passe doivent être identiques");
    }
});

