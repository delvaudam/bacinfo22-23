let cLink = $(".courseLink");
let mCourse = $("#mCourse");
let mForm = $("#mFormation");
let mPeriod = $("#mPeriod");
let mPrereq = $("#mPrereq");
let mDeter = $("#mDeter");

cLink.on("click", function () {
    let course = JSON.parse($(this).attr("data-bs-content"));
    mCourse.html(course.course_name);
    mForm.html(course.formation_name);
    mPeriod.html(course.periods);
    if (course.course_prereq) {
        mPrereq.html(course.course_prereq);
    } else {
        mPrereq.html("Aucun");
    }
    if (course.determining) {
        mDeter.html("Oui");
    } else {
        mDeter.html("Non");
    }
});