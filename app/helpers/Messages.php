<?php

namespace app\helpers;

class Messages
{
    /**
     * @var string
     */
    private static string $message;
    /**
     * @var string
     */
    private static string $level;

    /**
     * @param string $message
     * @param string $level
     * @return void
     * Set a message for the user
     */
    public static function setMessage(string $message, string $level): void
    {
        $_SESSION["message"] = "$message";
        $_SESSION["level"] = $level;
    }

    /**
     * @return string
     * Show the message set
     */
    public static function getMessage(): string
    {
        if (isset($_SESSION["message"]) && isset($_SESSION["level"])) {
            self::$message = $_SESSION["message"];
            self::$level = $_SESSION["level"];
            self::unsetMessage();
            echo "<div class='alert alert-" . self::$level . "'>" . self::$message . "</div>";
        }
        return 0;
    }

    /**
     * @return void
     * Unset the message
     */
    private static function unsetMessage(): void
    {
        unset($_SESSION["message"]);
        unset($_SESSION["level"]);
    }
}