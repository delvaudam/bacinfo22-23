<?php

namespace app\helpers;

use PDO;
use PDOException;

class Database
{
    /**
     * @var ?PDO
     */
    private static ?PDO $dbh = null;

    /**
     * @return ?PDO
     * Connect to the database
     */
    public static function connect(): ?PDO
    {
        if (!self::$dbh) {
            require_once __DIR__ . "/../../config.php";
            try {
                self::$dbh = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME . ";charset=utf8", DB_USER, DB_PASS);
                self::$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $exception) {
                die("Error : " . $exception->getMessage());
            }
        }
        return self::$dbh;
    }
}