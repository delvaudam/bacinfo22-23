<?php

namespace app\helpers;

use app\controllers\Role;
use app\models\User;

class Access
{
    /**
     * @param string $username
     * @return void
     * Check if the user is banned
     */
    public static function isBanned(string $username): void
    {
        if (User::hasRoleByLogin($username, Role::BANNED)) {
            Messages::setMessage("Utilisateur non désiré", "danger");
            header("location:index.php?page=login");
        }
    }

    /**
     * @param int $userId
     * @return void
     * Check if the user logged is accessing its own profile
     */
    public static function checkProfile(int $userId): void
    {
        if (empty($_SESSION["user"]) || $_SESSION["user"]->id != $userId) {
            header("location:index.php?page=login");
            die;
        }
    }

    /**
     * @param int $userid
     * @return int
     * Check if the user is admin
     */
    public static function isAdmin(int $userid): int
    {
        return User::hasRoleByUserId($userid, Role::ADMIN);
    }

    /**
     * @param string $username
     * @return int
     * Check if the user is active
     */
    public static function isActive(string $username): int
    {
        if (User::isActive($username)) {
            return 1;
        } else {
            return 0;
        }
    }
}