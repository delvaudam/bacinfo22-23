<?php

namespace app\helpers;

class Helper
{
    /**
     * @return string
     * Return the MaxFileSizeHumanReadable value in php.ini
     */
    public static function getMaxFileSizeHumanReadable(): string
    {
        return min(ini_get('post_max_size'), ini_get('upload_max_filesize'));
    }
}