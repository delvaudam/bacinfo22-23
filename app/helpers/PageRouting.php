<?php

namespace app\helpers;

use app\controllers\Country;
use app\controllers\Course;
use app\controllers\Formation;
use app\controllers\Role;
use app\controllers\User;

class PageRouting
{
    /**
     * @return void
     * Generate and return the home page
     */
    public static function getHomePage(): void
    {
        $title = "Accueil";
        $description = "Page d'accueil";

        require_once "views/homepage.php";
    }

    /**
     * @return void
     * Generate and return the contact page
     */
    public static function getContactPage(): void
    {
        $title = "Contactez-nous";
        $description = "Page de contact";

        require_once "views/contact.php";
    }

    /**
     * @return void
     * Generate and return the login page
     */
    public static function getLoginPage(): void
    {
        if (!empty($_POST["login"])) {
            $user = new User();
            $user->login();
        } else {
            $title = "Connectez-vous";
            $description = "Page de connexion";
            require_once "views/login.php";
        }
    }

    /**
     * @return void
     * Generate and return the signup page
     */
    public static function getSignupPage(): void
    {
        $title = "Inscrivez-vous";
        $description = "Page d'inscription";
        if (!empty($_POST["login"]) && !empty($_POST["password"]) && !empty($_POST["email"]) && isset($_POST["condition"])) {
            $user = new User();
            $user->create($_POST);
        } else {
            $c = new Country();
            $countries = $c->getAll();
            require_once "views/signup.php";
        }
    }

    /**
     * @return void
     * Generate and return the profile page of the logged user
     */
    public static function getProfilePage(): void
    {
        if (!isset($_SESSION["user"])) {
            Messages::setMessage("Veuillez vous connecter", "warning");
            header("location:index.php?page=login");
        } else {
            $user = new User();
            if (isset($_GET["export"]) && $_GET["export"] == $_SESSION["user"]->id) {
                $user->exportProfile($_SESSION["user"]->id);
            } elseif (isset($_GET["user"])) {
                $user->update($_GET["user"]);
            } elseif (isset($_GET["delete"]) && $_GET["delete"] == $_SESSION["user"]->id) {
                $user->delete($_SESSION["user"]->id);
            } else {
                $title = "Profil : " . $_SESSION["user"]->login;
                $description = "Votre profil utilisateur";
                $user = $user->get($_SESSION["user"]->id);
                $course = new Course();
                $courses = $course->getUserCourses($_SESSION["user"]->id);
                require_once "views/profile.php";
            }
        }
    }

    /**
     * @return void
     * Generate and return the error page
     */
    public static function getErrorPage(): void
    {
        $title = "Erreur : La page demandée n'existe pas";
        $description = $title;

        require_once "views/error.php";
    }

    /**
     * @return void
     * Logout the user
     */
    public static function getLogout(): void
    {
        $user = new User();
        $user->logout();
    }

    /**
     * @return void
     * Generate and return the courses page
     */
    public static function getCoursePage(): void
    {
        $c = new Course();
        $user = new User();
        if (isset($_SESSION["user"]) && isset($_GET["enrol"])) {
            $user->enrol($_SESSION["user"]->id, $_GET["enrol"]);
        } elseif (isset($_SESSION["user"]) && isset($_GET["unenroll"])) {
            $user->unenroll($_SESSION["user"]->id, $_GET["unenroll"]);
        } else {
            $title = "Nos cours";
            $description = "Liste de nos cours";
            $courses = $c->getAllComplete();
            require_once "views/course.php";
        }

    }

    /**
     * @return void
     * Generate and return the formations page
     */
    public static function getFormationPage(): void
    {
        $title = "Nos formations";
        $description = "Liste de nos formations";
        $formation = new Formation();
        $formations = $formation->getAll();
        require_once "views/formation.php";
    }

    /**
     * @return void
     * Generate and return the conditions page
     */
    public static function getConditionPage(): void
    {
        $title = "Nos conditions";
        $description = "Nos conditions d'utilisation";
        require_once "views/conditions.php";
    }

    /**
     * @return void
     * Generate and return the admin page
     */
    public static function getAdminPage(): void
    {
        if (!Access::isAdmin($_SESSION["user"]->id)) {
            header("location:index.php?page=login");
        } else {
            $u = new User();
            $c = new Course();
            $f = new Formation();

            if (isset($_POST["user"])) {
                $json = json_encode($u->get($_POST["user"]));
                echo $json;
                exit();
            }

            if (isset($_POST["c-name"]) && isset($_POST["c-code"])) {
                $c->create($_POST["c-name"], $_POST["c-code"]);
            } elseif (isset($_POST["f-name"])) {
                $f->create($_POST["f-name"]);
            }

            if (isset($_GET["ban"])) {
                $u->changeRole(Role::BANNED, $_GET["ban"]);
            }

            if (isset($_GET["deban"])) {
                $u->changeRole(Role::GUEST, $_GET["deban"]);
            } else {
                $title = "Administration";
                $description = "Administration";
                $users = $u->getAllUsers();
                $formations = $f->getAll();
                $courses = $c->getAll();
                require_once "views/admin.php";
            }
        }
    }
}