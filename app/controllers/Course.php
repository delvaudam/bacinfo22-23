<?php

namespace app\controllers;

use app\helpers\Messages;

class Course extends Controller
{
    /**
     * @param int $userid
     * @return array|int
     * Return the user's courses
     */
    public function getUserCourses(int $userid): array|int
    {
        return $this->model->getUserCourses($userid);
    }

    /**
     * @return array
     * Return all the courses with the formation associated, periods and prerequisite
     */
    public function getAllComplete(): array
    {
        return $this->model->getAllComplete();
    }

    /**
     * @param string $name
     * @param int $code
     * @return void
     * Create a course
     */
    public function create(string $name, int $code): void
    {
        if ($this->model->getByField("name", $name) || $this->model->getByField("code", $code)) {
            Messages::setMessage("Le cours existe déjà !", "warning");
            header("location:index.php?page=admin");
        }
        if ($this->model->create($name, $code)) {

            Messages::setMessage("Cours créé avec succès", "success");
        } else {
            Messages::setMessage("Echec de création du cours", "danger");
        }
        header("location:index.php?page=admin");
    }

    /**
     * @param int $course
     * @return int|null
     * Return the prerequisite of a course
     */
    public function getPrerequisite(int $course): int|null
    {
        return $this->model->getPrerequisite($course);
    }
}