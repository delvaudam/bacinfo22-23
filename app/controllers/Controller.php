<?php

namespace app\controllers;

use app\Models\Model;

abstract class Controller
{
    /**
     * @var int
     */
    protected int $id;
    /**
     * @var Model|mixed
     */
    protected Model $model;
    /**
     * @var mixed
     */
    protected mixed $data;
    /**
     * @var mixed|Model|null
     */
    protected static mixed $static_model = null;

    /**
     * @param int|null $id
     */
    public function __construct(int $id = null)
    {
        $class = get_called_class();
        $data = explode('\\', $class);
        $class = '\app\models\\' . end($data);
        $this->model = new $class();
        self::$static_model = $this->model;
        if (!empty($id)) {
            $this->id = $id;
            $this->data = $this->model->get($id);
        }
    }

    /**
     * @param int $id
     * @return object
     */
    public function get(int $id): object
    {
        return $this->model->get($id);
    }

    /**
     * @return array|bool
     */
    public function getAll(): array|bool
    {
        return $this->model->getAll();
    }
}