<?php

namespace app\controllers;

use app\helpers\Access;
use app\helpers\Helper;
use app\helpers\Messages;
use DateTime;
use DateTimeZone;
use Exception;

class User extends Controller
{
    /**
     * @var array|string[]
     * The authorized image formats
     */
    protected static array $images = [
        "image/gif",
        "image/jpeg",
        "image/png",
    ];

    /**
     * @param array $post_data
     * @return void
     * Create a user
     */
    public function create(array $post_data): void
    {
        if (!$post_data) {
            Messages::setMessage("Données de formulaire manquantes", "warning");
            header("location:index.php?page=sinscrire");
            exit;
        }

        if (!empty($post_data["login"]) && !empty($post_data["password"]) && !empty($post_data["password2"]) && ($post_data["password"] == $post_data["password2"])
            && filter_var($post_data["email"], FILTER_VALIDATE_EMAIL) && isset($post_data["condition"]) && isset($post_data["nationality"])
            && $post_data["nationality"] != "null" && isset($post_data["birthdate"]) && isset($post_data["address"]) && isset($post_data["telephone"])) {

            if (strlen($post_data["login"]) < 10 || strlen($post_data["password"]) < 10) {
                Messages::setMessage("Le login et le mot de passe doivent faire 10 caractères minimum", "warning");
            } else {

                $data = [
                    "login" => htmlspecialchars($post_data["login"]),
                    "password" => password_hash($post_data["password"], PASSWORD_DEFAULT),
                    "email" => htmlspecialchars($post_data["email"]),
                    "name" => htmlspecialchars($post_data["lastname"]),
                    "firstname" => htmlspecialchars($post_data["firstname"]),
                    "nationality" => htmlspecialchars($post_data["nationality"]),
                    "birthdate" => htmlspecialchars($post_data["birthdate"]),
                    "address" => htmlspecialchars($post_data["address"]),
                    "telephone" => htmlspecialchars($post_data["telephone"])
                ];

                if (!$this->model->getByLoginOrEmail($data["login"], $data["email"])) {
                    $userid = $this->model->create($data);
                    if ($userid) {
                        $this->model->addRole($userid, Role::GUEST);
                        $photo_message = false;
                        if (!$this->manageUploadedFile($userid)) {
                            $photo_message = "L'import de la photo a échoué";
                        }
                        Messages::setMessage($photo_message, "warning");
                    } else {
                        Messages::setMessage("La création du compte utilisateur a échoué", "warning");
                    }
                    Messages::setMessage("Utilisateur " . $data["login"] . " créé avec l'email " . $data["email"], "success");
                    header("location:index.php?page=login");
                    exit;
                } else {
                    Messages::setMessage("Un utilisateur avec ce login ou cette adresse email existe déjà", "warning");
                }
            }
        } else {
            Messages::setMessage("Données manquantes! Veuillez complèter entièrement le formulaire.", "warning");
        }
        header("location:index.php?page=sinscrire");
        exit;
    }

    /**
     * @param int $userId
     * @return void
     * Update user's profile
     */
    public function update(int $userId): void
    {
        if (!$_POST) {
            Messages::setMessage("Données de formulaire manquantes", "warning");
            header("location:index.php?page=profil");
            exit;
        }

        Access::checkProfile($userId);

        $update = false;

        $user = $this->get($userId);

        if (!empty($_POST['email']) && filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL) && $_POST['email'] != $user->email) {
            $email = trim($_POST['email']);
            if (!$this->model->getByLoginOrEmail('', $email)) {
                $user->email = $email;
                $update = true;
            } else {
                Messages::setMessage('Cette adresse email existe déjà!', 'danger');
            }
        }

        if (!empty($_POST['password']) && $_POST['password'] != $user->password) {
            $user->password = password_hash($_POST['password'], PASSWORD_DEFAULT);
            $update = true;
        }

        if (!empty($_FILES['photo']) && !empty($_FILES['photo']['name'])) {
            try {
                $user->photo = $this->manageUploadedFile($user->id);
            } catch (Exception $e) {
                Messages::setMessage($e->getMessage(), 'danger');
                header("location:index.php?page=sinscrire");
            }
            $update = true;
        }

        if ($update) {
            $now = new DateTime();
            $user->updated = $now->setTimezone(new DateTimeZone('Europe/Paris'))->format('Y-m-d H:i:s');
        }

        unset($user->role);
        unset($user->nationality);
        if ($update && $this->model->update($user)) {
            Messages::setMessage('La mise à jour du compte utilisateur a été effectuée avec succès', 'success');
        } else {
            Messages::setMessage('La mise à jour du compte utilisateur a échoué', 'danger');
        }
        header("location:index.php?page=profil");
    }

    /**
     * @return void
     */
    public function login(): void
    {
        if (!$_POST) {
            Messages::setMessage("Données de formulaire manquantes", "warning");
            header("location:index.php?page=login");
            exit;
        }

        if (!empty($_POST["login"]) && !empty($_POST["password"])) {
            $user = $this->model->getByField("login", htmlspecialchars($_POST["login"]));
            if (!$user) {
                Messages::setMessage("Cet utilisateur n'existe pas", "warning");
                header("location:index.php?page=login");
                exit;
            }

            Access::isBanned($_POST["login"]);

            if (!Access::isActive($_POST["login"])) {
                Messages::setMessage("Compte inactif", "warning");
                header("location:index.php?page=login");
                exit;
            }

            if (password_verify($_POST["password"], $user->password)) {
                if ($this->model->updateFieldById("lastLogin", "NOW()", $user->id)) {
                    session_destroy();
                    session_name("bacinfo" . date("Ymd"));
                    session_id(bin2hex(openssl_random_pseudo_bytes(32)));
                    session_start(["cookie_lifetime" => 3600]);
                    unset($user->password);
                    $_SESSION["user"] = $user;
                    header("location:index.php?page=profil");
                    exit;
                }
            } else {
                Messages::setMessage("Données de connexion incorrectes", "warning");
                header("location:index.php?page=login");
                exit;
            }
        }

    }

    /**
     * @return void
     */
    public function logout(): void
    {
        session_unset();
        session_destroy();
        session_write_close();
        header("Location: index.php");
        die;
    }

    /**
     * @param int $userid
     * @return mixed
     * Manage the uploaded file
     */
    protected function manageUploadedFile(int $userid): mixed
    {
        if (empty($_FILES["photo"]["tmp_name"]) && empty($_FILES["photo"]["name"])) {
            return 1;
        }
        $user = self::get($userid);
        if (!empty($_FILES["photo"]["tmp_name"]) && !empty($_FILES["photo"]["name"])
            && is_uploaded_file($_FILES["photo"]["tmp_name"]) && in_array($_FILES["photo"]["type"], self::$images)) {
            $photopath = "image" . DIRECTORY_SEPARATOR . "user" . DIRECTORY_SEPARATOR . "photo" . DIRECTORY_SEPARATOR;
            $imagepath = getcwd() . DIRECTORY_SEPARATOR . $photopath;

            if (!is_dir($imagepath)) {
                mkdir($imagepath, 0755, true);
            }

            $extension = pathinfo($_FILES["photo"]["name"], PATHINFO_EXTENSION);
            $destination = $imagepath . $userid . "." . $extension;
            $url = $photopath . $userid . "." . $extension;

            $move = move_uploaded_file($_FILES["photo"]["tmp_name"], $destination);
            if ($move) {
                if (!empty($user->photo) && $url != $user->photo && pathinfo($user->photo, PATHINFO_EXTENSION) != $extension) {
                    $existing_file = __DIR__ . DIRECTORY_SEPARATOR . $user->photo;
                    if (file_exists($existing_file)) {
                        unlink($existing_file);
                    }
                }

                $this->model->updateFieldById("photo", $url, $userid);

            } else {
                Messages::setMessage("Erreur de téléchargement", "warning");
                header("location:" . $_SERVER["HTTP_REFERER"]);
                die;
            }
        } else {
            $output_txt = "Le fichier a été refusé";
            if (!empty($_FILES['photo']['type']) && !in_array($_FILES['photo']['type'], self::$images)) {
                $output_txt .= '. Le format de fichier doit être ' . implode(' ou ', self::$images) . '.';
            }
            if ($_FILES["photo"]["error"]) {
                switch ($_FILES["photo"]["error"]) {
                    case UPLOAD_ERR_OK:
                        break;
                    case UPLOAD_ERR_INI_SIZE:
                    case UPLOAD_ERR_FORM_SIZE:
                        $output_txt .= ". La taille du fichier doit être inférieure à " . Helper::getMaxFileSizeHumanReadable() . ".";
                        break;
                    default:
                        $output_txt .= " pour une raison inconnue.";
                }
            }
            Messages::setMessage($output_txt, "warning");
            header("location:" . $_SERVER["HTTP_REFERER"]);
            die;
        }
        return $user->photo;
    }

    /**
     * @param int $userid
     * @return object
     * Return the user profile
     */
    public function getProfile(int $userid): object
    {
        $profile = $this->model->getProfile($userid);
        unset($profile->id);
        unset($profile->password);
        unset($profile->photo);
        return $profile;
    }

    /**
     * @param int $userid
     * @return void
     * Export the user profile
     */
    public function exportProfile(int $userid): void
    {
        $data = (array)$this->getProfile($userid);
        $filename = $data["login"] . "_" . time() . ".csv";
        $ressource = fopen($filename, 'w');
        fputcsv($ressource, $data);
        fclose($ressource);
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        echo file_get_contents($filename);
        unlink($filename);
    }

    /**
     * @return array
     * Return all users
     */
    public function getAllUsers(): array
    {
        return $this->model->getAllUsers();
    }

    /**
     * @param int $user
     * @param int $course
     * @return int
     * Check the enrollment of a user to a course
     */
    public function isEnrolled(int $user, int $course): int
    {
        return $this->model->isEnrolled($user, $course);
    }

    /**
     * @param int $user
     * @param int $course
     * @return void
     * Enrol a user to a course
     */
    public function enrol(int $user, int $course): void
    {
        $c = new Course();
        $prerequisite = $c->getPrerequisite($course);
        if ($prerequisite != null) {
            $isEnrolled = $this->isEnrolled($user, $prerequisite);
        } else {
            $isEnrolled = true;
        }
        if ($isEnrolled) {
            if ($this->model->enrol($user, $course)) {
                Messages::setMessage("Vous êtes maintenant inscrit", "success");
            } else {
                Messages::setMessage("Vous ne pouvez pas vous inscrire à ce cours", "warning");
            }
        } else {
            Messages::setMessage("Vous devez être inscrit au cours prérequis", "warning");
        }
        header("location:index.php?page=cours");
    }

    /**
     * @param int $user
     * @param int $course
     * @return void
     * Unenroll a user from a course
     */
    public function unenroll(int $user, int $course): void
    {
        $courseSanitized = htmlspecialchars($course);
        $this->model->unenroll($user, $courseSanitized);
        Messages::setMessage("Vous êtes désinscrit", "success");
        header("location:index.php?page=cours");
    }

    /**
     * @param int $user
     * @return void
     * Set the user to inactive
     */
    public function delete(int $user): void
    {
        $this->model->delete($user);
        $this->logout();
    }

    /**
     * @param int $roleid
     * @param int $userid
     * @return void
     * Allow the admin to change the role of a user
     */
    public function changeRole(int $roleid, int $userid): void
    {
        $roleid = htmlspecialchars($roleid);
        $userid = htmlspecialchars($userid);
        $this->model->changeRole($roleid, $userid);
        $user = $this->get($userid);
        $role = new Role();
        $roleName = $role->get($roleid);
        Messages::setMessage("Rôle de l'utilisateur " . $user->login . " changé à " . $roleName->name, "success");
        header("location:index.php?page=admin");
        exit;
    }
}