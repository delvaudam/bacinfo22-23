<?php

namespace app\controllers;

use app\helpers\Messages;

class Formation extends Controller
{
    /**
     * @param string $name
     * @return void
     * Create a formation
     */
    public function create(string $name): void
    {
        if ($this->model->create($name)) {
            Messages::setMessage("Formation créée avec succès", "success");
        } else {
            Messages::setMessage("Echec de création de la formation", "danger");
        }
        header("location:index.php?page=admin");
    }
}