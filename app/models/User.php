<?php

namespace app\models;

use app\helpers\Database;

class User extends Model
{
    /**
     * @param string $login
     * @param string $email
     * @return int
     * Return a user by its login or email
     */
    public static function getByLoginOrEmail(string $login, string $email): int
    {
        $stmt = self::$connect->prepare("SELECT COUNT(*) FROM user WHERE login = ? OR email = ?");
        $stmt->execute([$login, $email]);
        return $stmt->fetchColumn();
    }

    /**
     * @param array $data
     * @return int
     * Create a user
     */
    public static function create(array $data): int
    {
        $stmt = self::$connect->prepare("INSERT INTO user (login, password, email, lastname, firstname, nationality, birthdate, address, telephone, created, lastLogin, updated)
                                                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), NULL, NULL)");
        $stmt->execute(array_values($data));
        if ($stmt->rowCount()) {
            return self::$connect->lastInsertId();
        }
        return 0;
    }

    /**
     * @param int $userid
     * @param int $roleid
     * @return void
     * Add role to a user
     */
    public static function addRole(int $userid, int $roleid): void
    {
        $stmt = self::$connect->prepare("INSERT INTO user_role (userid, roleid, created) VALUES (?, ?, NOW())");
        $stmt->execute([$userid, $roleid]);
    }

    /**
     * @param string $login
     * @param int $roleid
     * @return int
     * Check if user has a specific role by login
     */
    public static function hasRoleByLogin(string $login, int $roleid): int
    {
        $stmt = self::$connect->prepare("SELECT COUNT(*) 
                                               FROM user u 
                                                   INNER JOIN user_role ur ON u.id = ur.userid 
                                               WHERE login = ? AND roleid = ?");
        $stmt->execute([$login, $roleid]);
        return $stmt->fetchColumn();
    }

    /**
     * @param int $userid
     * @param int $roleid
     * @return int
     * Check if user has a specific role by user id
     */
    public static function hasRoleByUserId(int $userid, int $roleid): int
    {
        self::$connect = Database::connect();
        $stmt = self::$connect->prepare("SELECT COUNT(*) FROM user_role WHERE userid = ? AND roleid = ?");
        $stmt->execute([$userid, $roleid]);
        return $stmt->fetchColumn();
    }

    /**
     * @return array
     * Return all users formatted
     */
    public static function getAllUsers(): array
    {
        $stmt = self::$connect->query("SELECT u.id as 'id', login, email, r.name as 'role', r.id as 'roleid', u.created as 'created', u.updated as 'updated', lastLogin, active
                                                 FROM user u 
                                                    INNER JOIN user_role ur on u.id = ur.userid 
                                                    INNER JOIN role r on r.id = ur.roleid");
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param int $userid
     * @return object
     * Return user's information for profile
     */
    public static function getProfile(int $userid): object
    {
        $stmt = self::$connect->prepare("SELECT u.id as 'id', login, password, email, lastname, firstname, c.nom_fr_fr as 'nationality', birthdate, address, telephone, u.created as 'created', lastLogin, updated, r.name as 'role', photo 
                                               FROM user u 
                                                   INNER JOIN user_role ur on u.id = ur.userid
                                                   INNER JOIN role r on ur.roleid = r.id
                                                   INNER JOIN country c ON u.nationality = c.id
                                               WHERE u.id = ?");
        $stmt->execute([$userid]);
        return $stmt->fetchObject();
    }

    /**
     * @param int $user
     * @param int $course
     * @return int
     * Enrol a user to a course
     */
    public static function enrol(int $user, int $course): int
    {
        $stmt = self::$connect->prepare("INSERT INTO user_course (userid, courseid, created) VALUES (?, ?, NOW())");
        $stmt->execute([$user, $course]);
        if ($stmt->rowCount()) {
            return self::$connect->lastInsertId();
        }
        return 0;
    }

    /**
     * @param int $user
     * @param int $course
     * @return void
     * Unroll a user from a course
     */
    public static function unenroll(int $user, int $course): void
    {
        $stmt = self::$connect->prepare("DELETE FROM user_course WHERE userid = ? AND courseid = ?");
        $stmt->execute([$user, $course]);
    }

    /**
     * @param int $user
     * @param int $course
     * @return int
     * Check if a user is enrolled to a course
     */
    public static function isEnrolled(int $user, int $course): int
    {
        $stmt = self::$connect->prepare("SELECT COUNT(*) FROM user_course WHERE userid = ? AND courseid = ?");
        $stmt->execute([$user, $course]);
        return $stmt->fetchColumn();
    }

    /**
     * @param int $user
     * Set a user to inactive
     */
    public static function delete(int $user)
    {
        $stmt = self::$connect->prepare("UPDATE user SET active = FALSE WHERE id = ?");
        $stmt->execute([$user]);
    }

    /**
     * @param string $username
     * @return int
     * Check if a user is active
     */
    public static function isActive(string $username): int
    {
        $stmt = self::$connect->prepare("SELECT COUNT(*) FROM user WHERE login = ? AND active = true");
        $stmt->execute([$username]);
        return $stmt->fetchColumn();
    }

    /**
     * @param int $role
     * @param int $userid
     * @return void
     * Change the role of a user
     */
    public static function changeRole(int $role, int $userid): void
    {
        $stmt = self::$connect->prepare("UPDATE user_role SET roleid = ? WHERE userid = ?");
        $stmt->execute([$role, $userid]);
    }
}