<?php

namespace app\models;

class Formation extends Model
{
    /**
     * @param string $name
     * @return int
     * Create a formation
     */
    public static function create(string $name): int
    {
        $stmt = self::$connect->prepare("INSERT INTO formation (name) VALUE (?)");
        $stmt->execute([$name]);
        if ($stmt->rowCount()) {
            return self::$connect->lastInsertId();
        }
        return 0;
    }
}