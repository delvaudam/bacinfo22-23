<?php

namespace app\models;

class Country extends Model
{
    /**
     * @return array|bool
     * Return all the countries
     */
    public static function getAll(): array|bool
    {
        return self::$connect->query("SELECT id, nom_fr_fr FROM country")->fetchAll();
    }
}