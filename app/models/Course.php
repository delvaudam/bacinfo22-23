<?php

namespace app\models;

use PDO;

class Course extends Model
{
    /**
     * @param int $userid
     * @return array|bool
     * Return the user's courses
     */
    public static function getUserCourses(int $userid): array|bool
    {
        $stmt = self::$connect->prepare("SELECT c.id as 'id', name, code
                                               FROM course c 
                                                   INNER JOIN user_course uc ON c.id = uc.courseid
                                               WHERE userid = ?");
        $stmt->execute([$userid]);
        return $stmt->fetchAll();
    }

    /**
     * @return array|false
     * Return all the courses with the formation associated, periods and prerequisite
     */
    public static function getAllComplete(): array|bool
    {
        $sql = "SELECT c.id as courseid, 
                f.name as formation_name, 
                c.name as course_name, 
                c.code as code,
                fc.period as periods, 
                fc.determining as det, 
                c2.name as course_prereq
               FROM formation_course fc
               JOIN formation f ON f.id = fc.formationid
               JOIN course c ON c.id = fc.courseid
               LEFT JOIN formation_course fc2 ON fc2.id = fc.prereq
               LEFT JOIN course c2 ON c2.id = fc2.courseid
               ORDER BY fc.formationid";
        $stmt = self::$connect->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param string $name
     * @param int $code
     * @return int
     * Create a course
     */
    public static function create(string $name, int $code): int
    {
        $stmt = self::$connect->prepare("INSERT INTO course (name, code) VALUES (?, ?)");
        $stmt->execute([$name, $code]);
        if ($stmt->rowCount()) {
            return self::$connect->lastInsertId();
        }
        return 0;
    }

    /**
     * @param int $course
     * @return int|null
     * Return the prerequisite of a course
     */
    public static function getPrerequisite(int $course): int|null
    {
        $stmt = self::$connect->prepare("SELECT prereq FROM formation_course WHERE courseid = ?");
        $stmt->execute([$course]);
        return $stmt->fetchColumn();
    }
}