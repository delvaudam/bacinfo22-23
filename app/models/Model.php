<?php

namespace app\models;

use PDO;

abstract class Model
{
    /**
     * @var PDO|null
     */
    protected static ?PDO $connect;

    /**
     * Set the connection to the database for the models
     */
    public function __construct()
    {
        global $connect;
        self::$connect = $connect;
    }

    /**
     * @param int $id
     * @return object
     * Return the object by id by model's name
     */
    public static function get(int $id): object
    {
        $stmt = self::$connect->prepare("SELECT * FROM " . self::getClassName() . " WHERE id = ?");
        $stmt->execute([$id]);
        return $stmt->fetchObject();
    }

    /**
     * @param string $field
     * @param string $value
     * @return object|bool
     * Return the object by field by model's name
     */
    public static function getByField(string $field, string $value): object|bool
    {
        $request = self::$connect->prepare("SELECT * FROM " . self::getClassName() . " WHERE $field = ?");
        $request->execute([$value]);
        return $request->fetchObject();
    }

    /**
     * @return string
     * Return current model's name
     */
    protected static function getClassName(): string
    {
        $class = get_called_class();
        $data = explode('\\', $class);
        return strtolower(end($data));
    }

    /**
     * @param string $field
     * @param string $value
     * @param int $id
     * @return int|bool
     * Update a field by id
     */
    public static function updateFieldById(string $field, string $value, int $id): int|bool
    {
        if (!in_array($field, self::getColumns())) {
            return false;
        }

        if ($value != 'NOW()') {
            $val = '?';
            $params = [$value, $id];
        } else {
            $val = $value;
            $params = [$id];
        }

        $request = self::$connect->prepare("UPDATE " . self::getClassName() . " SET $field = $val WHERE id = ?");
        $request->execute($params);
        return $request->rowCount();
    }

    /**
     * @param object $object
     * @return int
     * Update an object
     */
    public static function update(object $object): int
    {
        if (empty($object->id)) {
            return false;
        }
        $params = array_values(get_object_vars($object));
        $params[] = $object->id;
        $setFields = self::getSelectFields($object, ',', ' = ?');
        $query = "UPDATE " . self::getClassName() . " SET $setFields WHERE id = ?";
        $request = self::$connect->prepare($query);
        $request->execute($params);
        return $request->rowCount();
    }

    /**
     * @return array
     * Return the columns of a db table
     */
    protected static function getColumns(): array
    {
        $columns = [];
        $cols = self::$connect->query("DESCRIBE " . self::getClassName(), PDO::FETCH_OBJ);
        foreach ($cols as $col) {
            $columns[] = $col->Field;
        }
        return $columns;
    }

    /**
     * @return array|bool
     * Return all from a db table
     */
    public static function getAll(): array|bool
    {
        return self::$connect->query("SELECT * FROM " . self::getClassName())->fetchAll();
    }

    /**
     * @param object $fields
     * @param string $concat [, when select or update; AND, OR, etc... when WHERE]
     * @param string $addtxt [suffix for each query element, for example = ? for each field of the where clause in a prepared query]
     * @return string
     * Generate the fields of a select where request
     */
    protected static function getSelectFields(object $fields, string $concat, string $addtxt = ''): string
    {
        $return = '';
        $nbr = 0;
        foreach ($fields as $key => $value) {
            if ($nbr > 0) {
                if ($value == 'NOW()') {
                    $return .= $concat . $key . '=NOW()';
                } else {
                    $return .= $concat . $key . $addtxt;
                }
            } else {
                $return .= $key . $addtxt;
            }
            $nbr++;
        }
        return $return;
    }
}