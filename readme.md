# Installation

## Prérequis

* Php 8.0+
* MySQL 5.7+

## Fichier de configuration

Le fichier config-dist.php doit être copié et renommé en config.php.

Les paramètres de connexion à la DB doivent être indiqués dans ce nouveau fichier.

## Migrations

Une fois la base de données et le fichier de configuration créés, il est nécessaire d'exécuter les migrations DB.

Pour ce faire, utilisez le script migration.php, à exécuter en commande CLI. La table 'user' doit être migrée après
la table 'userrole' et la table 'formationcourse' après les tables 'formation' et 'course'
Un utilisateur avec le rôle administrateur sera créé durant la migration depuis le fichier user.sql et userrole.sql
(Login admin, mot de passe admin).

## Notes
L'inscription à la newsletter et le formulaire de contact ne sont pas fonctionnels.
Le dossier vendor présent dans assets est propre au template HTML utiliser.