CREATE TABLE IF NOT EXISTS `user_role`
(
    `id`      BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `userid`  INT(10) UNSIGNED    NOT NULL,
    `roleid`  INT(10) UNSIGNED    NOT NULL,
    `created` datetime            NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `userid` (`userid`, `roleid`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

INSERT INTO user_role (userid, roleid, created)
VALUES (1, 1, NOW());