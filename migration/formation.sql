CREATE TABLE IF NOT EXISTS `formation`
(
    `id`   bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` varchar(255)        NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE InnoDB
  DEFAULT CHARSET = utf8mb4;

INSERT INTO `formation` (`id`, `name`)
VALUES (1, 'BES Webdeveloper'),
       (2, 'Bachelier en informatique de gestion');