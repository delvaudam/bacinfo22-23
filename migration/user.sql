CREATE TABLE `user`
(
    `id`          INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `login`       VARCHAR(50)      NOT NULL,
    `password`    VARCHAR(255)     NOT NULL,
    `email`       VARCHAR(100)     NOT NULL,
    `lastname`    VARCHAR(100)     NOT NULL,
    `firstname`   VARCHAR(100)     NOT NULL,
    `nationality` SMALLINT(5)      NOT NULL,
    `birthdate`   DATE             NOT NULL,
    `address`     VARCHAR(255)     NOT NULL,
    `telephone`   VARCHAR(25)      NOT NULL,
    `photo`       VARCHAR(255),
    `created`     DATETIME         NOT NULL,
    `lastLogin`   DATETIME,
    `updated`     DATETIME,
    `active`      BOOLEAN DEFAULT TRUE,
    PRIMARY KEY (`id`),
    UNIQUE KEY (`login`),
    UNIQUE KEY (`email`)
) ENGINE InnoDB
  DEFAULT CHARSET utf8;

INSERT INTO `user` (`id`, `login`, `password`, `email`, `lastname`, `firstname`, `nationality`, `birthdate`, `address`,
                    `telephone`, `photo`, `created`, `lastLogin`, `updated`, `active`)
VALUES (1, 'admin', '$2y$10$.WO8LNDDNjIXpXZMcyoA4u2QuOR3eOgUImq9v8.xwW3DDPqKV5W1u', 'admin@istrateur.com', 'Admin',
        'Istrateur', 1, '1950-01-01', 'test', '012/34.56.78', NULL, '2023-01-09 20:48:44', '2023-01-26 18:17:28',
        '2023-01-17 19:09:17', 1);