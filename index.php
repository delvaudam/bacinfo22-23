<?php
// Session initialization
session_name("bacinfo" . date("Ymd"));
session_start(["cookie_lifetime" => 3600]);

// Class autoload
spl_autoload_register(function ($className) {
    $ds = DIRECTORY_SEPARATOR;
    $dir = __DIR__;
    $className = str_replace('\\', $ds, $className);
    $file = "{$dir}{$ds}{$className}.php";
    if (is_readable($file)) require_once $file;
});

// Database connection
$connect = app\helpers\Database::connect();

// Setting default page
if (!isset($_GET["page"])) $_GET["page"] = "accueil";

// Pages index
switch ($_GET["page"]) {
    case "accueil":
        app\helpers\PageRouting::getHomePage();
        break;
    case "contact":
        app\helpers\PageRouting::getContactPage();
        break;
    case "sinscrire":
        app\helpers\PageRouting::getSignupPage();
        break;
    case "login":
        app\helpers\PageRouting::getLoginPage();
        break;
    case "logout":
        app\helpers\PageRouting::getLogout();
        break;
    case "profil":
        app\helpers\PageRouting::getProfilePage();
        break;
    case "cours":
        app\helpers\PageRouting::getCoursePage();
        break;
    case "formations":
        app\helpers\PageRouting::getFormationPage();
        break;
    case "conditions":
        app\helpers\PageRouting::getConditionPage();
        break;
    case "admin":
        app\helpers\PageRouting::getAdminPage();
        break;
    default:
        app\helpers\PageRouting::getErrorPage();
}
